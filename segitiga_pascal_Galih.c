#include <stdio.h>

unsigned long long factorial(int n) {
    if (n == 0 || n == 1) {
        return 1;
    } else {
        return n * factorial(n - 1);
    }
}


unsigned long long combination(int n, int r) {
    return factorial(n) / (factorial(n - r) * factorial(r));
}

int main() {
    int N;
    printf("Masukkan N: ");
    scanf("%d", &N);

    for (int i = 0; i < N; i++) {
        for (int j = 0; j <= i; j++) {
            printf("%llu ", combination(i, j));
        }
        printf("\n");
    }

return.0;
}
/*
Masukkan N: 5
    1 
   1 1 
  1 2 1 
 1 3 3 1 
1 4 6 4 1 
*/
